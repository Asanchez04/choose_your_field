from django.shortcuts import render
from .forms import RegisterForm
from django.contrib.auth import get_user_model
from django.views.generic import (
    TemplateView,
    CreateView,
    ListView,
    UpdateView,
    DetailView,
    DeleteView,
)
from django.contrib.messages.views import SuccessMessageMixin
from apps.users.tasks import send_email_task


class RegisterView(SuccessMessageMixin, CreateView):
    model = get_user_model()
    template_name = "users/user_register.html"
    form_class = RegisterForm
    success_url = "/accounts/login/"
    success_message = "your account was created successfully"

def send_mail(request):
    email= request.POST.get('email', '')
    if email:
        send_email_task(email)
    else:
        send_email_task()
    
    return render(request, 'index.html', )

