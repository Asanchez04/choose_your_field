from django.test import Client, TestCase


class TestViews(TestCase):
    def test_create_user(self):
        client = Client()
        data = {
            "username": "test_user",
            "email": "test@gmail.com",
            "password1": "test_password",
            "password2": "test_password",
            "first_name": "test_first_name",
            "last_name": "test_last_name",
        }
        response = client.get("/register_user/")
        self.assertEqual(response.status_code, 200)
