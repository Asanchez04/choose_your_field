from django.db import IntegrityError
from django.test import TestCase, TransactionTestCase
from apps.users.models import User  
from django.core import mail
from apps.users.tasks import send_email_task


class TestUserModels(TestCase):
    def test_create_user(self):
        User.objects.create(
            username="test_user",
            email="test_user@gmail.com",
            password="test_password",
            first_name="test",
            last_name="user",
        )

    def test_email_mandatory(self):
        with self.assertRaises(IntegrityError):
            User.objects.create(
                username="test_user",
                password="test_password",
                first_name="test",
                last_name="user",
            )

    def test_username_mandatory(self):
        with self.assertRaises(IntegrityError):
            User.objects.create(
                email="test_user@gmail.com",
                password="test_password",
                first_name="test",
                last_name="user",
            )

class EmailTest(TestCase):
    def test_send_email(self):
        send_email_task()

         # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)
        # Verify that the subject of the first message is correct.
        self.assertEqual(mail.outbox[0].subject, 'Email Prueba create users')

        
    def test_send_email_mandatory(self):
        send_email_task()

        with self.assertRaises(AssertionError):
             # Verify that the subject of the first message is incorrect.
            self.assertEqual(mail.outbox[0].subject, 'Email Prueba create user')
            # Test that one message hasn't been sent.
            self.assertEqual(len(mail.outbox), 0)