from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager as DjangoUserManager


class UserManager(DjangoUserManager):
    """_
        create model UserManager

    Args:
        DjangoUserManager (_type_): Create and save a user with the given username, email, and password.
    """

    def create_user(
        self,
        username,
        type_user="",
        first_name="",
        last_name="",
        email="",
        password=None,
    ):
        return super().create_user(
            username,
            email,
            password,
            type_user=type_user,
            first_name=first_name,
            last_name=last_name,
        )

    def create_superuser(
        self, username, first_name="", last_name="", email="", password=None
    ):
        return super().create_superuser(
            username,
            email,
            password,
            first_name=first_name,
            last_name=last_name,
        )


class User(AbstractUser):

    address = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=False, null=False, default=None)

    objects = UserManager()

    def __str__(self):
        return self.username
