from django.contrib import admin
from .models import User
from django.contrib.auth.models import Permission


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "first_name", "last_name", "email", "username")
    search_fields = ("id", "first_name", "email", "username")


admin.site.register(Permission)
