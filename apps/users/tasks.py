from choose_your_field.celery import app
from django.core.mail import BadHeaderError, send_mail
from .models import User
from utils.utils import Utils
from django.conf import settings


@app.task
def load_users():
    data = Utils().read_csv()
    for row in data:
        User.objects.get_or_create(
            username=row["username"],
            email=row["email"],
            first_name=row["first_name"],
            last_name=row["last_name"],
            password=row["password"],
        )
    
    
@app.task
def send_email_task(to_email='alanpiemailtask@gmail.com'):
    send_mail(
        subject='Email Prueba create users',
        message='Hello bro, how are you?, all users have been created',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[to_email]
    )
