import datetime
from django.db import IntegrityError
from django.test import TestCase, TransactionTestCase
from apps.field.models import Field, Reservation, Invoice
from apps.users.models import User


class TestFieldModels(TestCase):
    def test_create_field(self):
        Field.objects.create(
            name="test_field",
            price_hour=10000,
            type=Field.TypeChoices.Five,
            size=10,
            status_field=Field.StatusFieldChoices.Open,
        )

    def test_name_mandatory(self):
        with self.assertRaises(IntegrityError):
            Field.objects.create(
                price_hour=10000,
                type=Field.TypeChoices.Five,
                size=10,
                status_field=Field.StatusFieldChoices.Open,
            )

    def test_price_hour_mandatory(self):
        with self.assertRaises(IntegrityError):
            Field.objects.create(
                name="test_field",
                type=Field.TypeChoices.Five,
                size=10,
                status_field=Field.StatusFieldChoices.Open,
            )

    def test_size_mandatory(self):
        with self.assertRaises(IntegrityError):
            Field.objects.create(
                name="test_field",
                price_hour=10000,
                type=Field.TypeChoices.Five,
                status_field=Field.StatusFieldChoices.Open,
            )


class TestReservationModels(TransactionTestCase):
    def test_create_reservation(self):
        user = User.objects.create(
            username="test_user",
            email="test_user@gmail.com",
            password="test_password",
            first_name="test_first_name",
            last_name="test_last_name",
        )
        field = Field.objects.create(
            name="test_field",
            price_hour=10000,
            type=Field.TypeChoices.Five,
            size=10,
            status_field=Field.StatusFieldChoices.Open,
        )
        Reservation.objects.create(
            id_user=user,
            id_field=field,
            date=datetime.datetime.now(),
            time_from=datetime.time(hour=10, minute=0, second=0),
            time_to=datetime.time(hour=12, minute=0, second=0),
        )

    def test_time_from_mandatory(self):
        user = User.objects.create(
            username="test_user",
            email="test_user@gmail.com",
            password="test_password",
            first_name="test_first_name",
            last_name="test_last_name",
        )
        field = Field.objects.create(
            name="test_field",
            price_hour=10000,
            type=Field.TypeChoices.Five,
            size=10,
            status_field=Field.StatusFieldChoices.Open,
        )

        with self.assertRaises(IntegrityError):
            Reservation.objects.create(
                id_user=user,
                id_field=field,
                date=datetime.datetime.now(),
                time_to=datetime.time(hour=12, minute=0, second=0),
            )

    def test_time_to_mandatory(self):
        user = User.objects.create(
            username="test_user",
            email="test_user@gmail.com",
            password="test_password",
            first_name="test_first_name",
            last_name="test_last_name",
        )
        field = Field.objects.create(
            name="test_field",
            price_hour=10000,
            type=Field.TypeChoices.Five,
            size=10,
            status_field=Field.StatusFieldChoices.Open,
        )

        with self.assertRaises(IntegrityError):
            Reservation.objects.create(
                id_user=user,
                id_field=field,
                date=datetime.datetime.now(),
                time_from=datetime.time(hour=10, minute=0, second=0),
            )


class TestInvoiceModels(TransactionTestCase):
    def test_create_invoice(self):
        user = User.objects.create(
            username="test_user",
            email="test_user@gmail.com",
            password="test_password",
            first_name="test_first_name",
            last_name="test_last_name",
        )
        field = Field.objects.create(
            name="test_field",
            price_hour=10000,
            type=Field.TypeChoices.Five,
            size=10,
            status_field=Field.StatusFieldChoices.Open,
        )
        reservation = Reservation.objects.create(
            id_user=user,
            id_field=field,
            date=datetime.datetime.now(),
            time_from=datetime.time(hour=10, minute=0, second=0),
            time_to=datetime.time(hour=12, minute=0, second=0),
        )
        date = datetime.datetime.now()
        date_from = datetime.datetime(
            date.year,
            date.month,
            date.day,
            reservation.time_from.hour,
            reservation.time_from.minute,
            reservation.time_from.second,
        )
        date_to = datetime.datetime(
            date.year,
            date.month,
            date.day,
            reservation.time_to.hour,
            reservation.time_to.minute,
            reservation.time_to.second,
        )
        Invoice.objects.create(
            id_reservation=reservation,
            total_amount=field.price_hour
            * (date_to - date_from).total_seconds()
            / 3600,
        )
