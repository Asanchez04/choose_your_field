from django.contrib.messages.views import SuccessMessageMixin
from .forms import CreateFieldForm
from .models import Field
from django.shortcuts import render, redirect
from .models import Reservation, Invoice
from .forms import ReservationForm
from django.views.generic import (
    TemplateView,
    CreateView,
    ListView,
    UpdateView,
    DetailView,
    DeleteView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class RegisterFieldView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = ("field.add_field",)
    model = Field
    template_name = "fields/field_register.html"
    form_class = CreateFieldForm
    success_url = "/fields/home"
    success_message = "your field was created successfully"

    def handle_no_permission(self):
        return redirect("/fields/home")


class ListFieldView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    permission_required = ("field.view_field",)
    model = Field
    template_name = "field/field_list.html"
    context_object_name = "fields"

    def handle_no_permission(self):
        return redirect("/fields/home")


class CreateReservationView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = ("reservation.add_reservation",)
    model = Reservation
    template_name = "field/detail.html"
    form_class = ReservationForm

    def post(self, request, *args, **kwargs):
        user = request.user
        field = Field.objects.get(pk=kwargs["pk"])
        Reservation.objects.create(
            id_user=user,
            id_field=field,
            time_from=request.POST["time_from"],
            time_to=request.POST["time_to"],
        )
        return redirect("field:home")

    def handle_no_permission(self):
        return redirect("/fields/home")


class ListReservationView(LoginRequiredMixin, ListView):
    model = Reservation


class HomeView(TemplateView):
    """
    This view shows the home page of the app.
    """

    template_name = "index.html"


def user_list(request):
    """
    You can show all fields of your database
    """
    field = Field.objects.all()

    context = {"fields": field}
    return render(request, "field/field_list.html", context)

    template_name = "index.html"
