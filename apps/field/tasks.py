import datetime
from choose_your_field.celery import app
from .models import Reservation
from django.conf import settings


@app.task
def close_lost_reservations():

    print("close_lost_reservations")
    reservations = Reservation.objects.filter(
        date=datetime.datetime.now().date(),
        status_reserve=Reservation.ReservationStatusChoices.UnPaid,
    )
    for reservation in reservations:
        if (
            reservation and (datetime.now - reservation.date).total_seconds()
        ) / 60.0 > settings.MAX_MINUTES_RESERVE:
            reservation.status_reserve = Reservation.ReservationStatusChoices.Canceled
            reservation.save()
