from secrets import choice
from django.db import models
from apps.users.models import User


class Field(models.Model):
    class TypeChoices(models.IntegerChoices):
        Five = 0, "5"
        Eigth = 1, "8"
        Eleven = 2, "11"

    class StatusFieldChoices(models.IntegerChoices):
        Open = 0, "Abierto"
        Close = 1, "Cerrado"

    name = models.CharField(max_length=255, null=False, blank=False, default=None)
    price_hour = models.IntegerField(null=False, blank=False, default=None)
    type = models.IntegerField(choices=TypeChoices.choices, default=TypeChoices.Five)
    size = models.IntegerField(null=False, blank=False, default=None)
    status_field = models.IntegerField(
        choices=StatusFieldChoices.choices, default=StatusFieldChoices.Open
    )


class Reservation(models.Model):
    class ReservationStatusChoices(models.IntegerChoices):
        UnPaid = 0, "No pagado"
        Paid = 1, "Pagado"
        Canceled = 2, "Cancelado"

    id_user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    id_field = models.ForeignKey(
        Field, on_delete=models.CASCADE, null=False, blank=False
    )
    date = models.DateTimeField(auto_now_add=True)
    time_from = models.TimeField(null=False, blank=False, default=None)
    time_to = models.TimeField(null=False, blank=False, default=None)
    status_reserve = models.IntegerField(
        choices=ReservationStatusChoices.choices,
        default=ReservationStatusChoices.UnPaid,
    )


class Invoice(models.Model):
    id_reservation = models.OneToOneField(Reservation, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    total_amount = models.IntegerField()
