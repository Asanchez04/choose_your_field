from django import forms
from .models import Field
from .models import Reservation


class CreateFieldForm(forms.ModelForm):

    name = forms.CharField(
        label="Name Field", widget=forms.TextInput(attrs={"class": "form-service"})
    )
    type = forms.IntegerField(
        label="Type Field",
        widget=forms.Select(
            choices=Field.TypeChoices.choices, attrs={"class": "form-service"}
        ),
    )
    size = forms.IntegerField(label="Size Field")
    status_field = forms.IntegerField(
        label="Status Field",
        widget=forms.Select(
            choices=Field.StatusFieldChoices.choices, attrs={"class": "form-service"}
        ),
    )
    price_hour = forms.IntegerField(label="Price Hour")

    class Meta:
        model = Field
        fields = ["name", "type", "size", "price_hour", "status_field"]


class ReservationForm(forms.ModelForm):

    time_from = forms.TimeField(widget=forms.TimeInput())
    time_to = forms.TimeField(widget=forms.TimeInput())
    status_reserve = forms.ChoiceField(
        widget=forms.Select(), choices=Reservation.ReservationStatusChoices.choices
    )

    class Meta:
        model = Reservation
        fields = ["time_from", "time_to", "status_reserve"]
