from django.contrib import admin

from .models import Field, Invoice, Reservation


admin.site.register(Field)
admin.site.register(Invoice)
admin.site.register(Reservation)
