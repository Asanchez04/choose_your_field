from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import (
    HomeView,
    CreateReservationView,
    RegisterFieldView,
    ListFieldView,
    user_list,
)

app_name = "field"

urlpatterns = [
    path("home/", HomeView.as_view(), name="home"),
    path("register_field/", RegisterFieldView.as_view(), name="register_field"),
    path("list_field/", user_list, name="list_field"),
    path(
        "create_reservation/<int:pk>",
        CreateReservationView.as_view(),
        name="create_reservation",
    ),
]
