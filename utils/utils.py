import csv


class Utils:
    def __init__(self):
        pass

    def read_csv(self):
        data = []
        with open("files/csv_user.csv") as f:
            reader = csv.DictReader(f)
            for row in reader:
                data.append(row)
        return data
